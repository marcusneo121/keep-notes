import 'package:flutter/material.dart';
import 'package:keep_notes/database/database.dart';
import 'package:provider/provider.dart';

class NoteDetail extends StatefulWidget {
  final String title;
  final NoteCompanion noteComp;
  const NoteDetail({Key? key, required this.title, required this.noteComp})
      : super(key: key);

  @override
  _NoteDetailState createState() => _NoteDetailState();
}

class _NoteDetailState extends State<NoteDetail> {
  late NoteDatabase appDatabase;
  final purpleColor = const Color(0xff6688FF);
  final darkTextColor = const Color(0xff1F1A3D);
  final lightTextColor = const Color(0xff999999);
  final textFieldColor = const Color(0xffF5F6FA);
  final borderColor = const Color(0xffD9D9D9);

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  @override
  void initState() {
    titleController.text = widget.noteComp.title.value;
    descriptionController.text = widget.noteComp.description.value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    appDatabase = Provider.of<NoteDatabase>(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.chevron_left_outlined,
            color: Colors.black,
          ),
        ),
        title: Text(
          widget.title,
          style: const TextStyle(
            color: Colors.black,
          ),
        ),
        actions: [
          // IconButton(
          //   onPressed: () {},
          //   icon: const Icon(
          //     Icons.save,
          //     color: Colors.black,
          //   ),
          // ),
          IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.delete,
              color: Colors.black,
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        shape: const CircleBorder(
          side: BorderSide(color: Color(0xFFFFC500), width: 2),
        ),
        backgroundColor: const Color(0xFFFFC500),
        child: const Icon(
          Icons.save,
          color: Colors.black,
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            getTextField(
                hint: "Title",
                controller: titleController,
                maxLength: 30,
                maxLines: 2,
                minLines: 1),
            const SizedBox(
              height: 16,
            ),
            getTextField(
                hint: "Description",
                controller: descriptionController,
                maxLength: 255,
                maxLines: 8,
                minLines: 7),
          ],
        ),
      ),
    );
  }

  Widget getTextField(
      {required String hint,
      required TextEditingController controller,
      required int maxLength,
      required int minLines,
      required int maxLines}) {
    return TextField(
      controller: controller,
      maxLength: maxLength,
      maxLines: maxLines,
      minLines: minLines,
      decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: const BorderSide(color: Colors.transparent, width: 0),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: const BorderSide(color: Colors.transparent, width: 0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: const BorderSide(color: Color(0xFFFFC500), width: 2),
          ),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 16, vertical: 14),
          filled: true,
          fillColor: textFieldColor,
          hintText: hint,
          hintStyle: const TextStyle(
            fontSize: 16,
            fontFamily: 'Nunito',
            fontWeight: FontWeight.w400,
          )),
    );
  }
}
