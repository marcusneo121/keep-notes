import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:keep_notes/database/database.dart';
import 'package:keep_notes/screens/note_detail.dart';
import 'package:provider/provider.dart';

class NoteList extends StatefulWidget {
  const NoteList({Key? key}) : super(key: key);

  @override
  _NoteListState createState() => _NoteListState();
}

class _NoteListState extends State<NoteList> {
  late NoteDatabase database;

  @override
  Widget build(BuildContext context) {
    database = Provider.of<NoteDatabase>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: FutureBuilder<List<NoteData>>(
        future: _getNotes(),
        builder: (context, snap) {
          if (snap.hasData) {
            List<NoteData>? noteList = snap.data;
            if (noteList != null) {
              if (noteList.isEmpty) {
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.all(14.0),
                    child: Text(
                      'No Notes Added, Click on + to add new note.',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ),
                );
              } else {
                return noteListUI(noteList);
              }
            }
          } else if (snap.hasError) {
            return Center(
              child: Text(
                snap.error.toString(),
                style: Theme.of(context).textTheme.bodyText2,
              ),
            );
          }
          return Center(
            child: Text(
              '{Default}',
              style: Theme.of(context).textTheme.bodyText2,
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _goToDetail(
              'Add Note',
              const NoteCompanion(
                  title: Value(''),
                  description: Value(''),
                  color: Value(1),
                  priority: Value(1)));
        },
        shape: const CircleBorder(
          side: BorderSide(color: Color(0xFFFFC500), width: 2),
        ),
        backgroundColor: const Color(0xFFFFC500),
        child: const Icon(
          Icons.add,
          color: Colors.black,
        ),
      ),
    );
  }

  Future<List<NoteData>> _getNotes() async {
    return await database.getNotes();
  }

  Widget noteListUI(List<NoteData> noteList) {
    return Container();
  }

  _goToDetail(String title, NoteCompanion noteComp) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => NoteDetail(
          title: title,
          noteComp: noteComp,
        ),
      ),
    );
  }
}
